﻿using UnityEngine;
using System.Collections;

public class CounterController : MonoBehaviour {
	

	public const int INITAL_LIFE_POINTS 		= 8000;
	public const int LOWER_BOUND_LIFE_POINTS	= 0;

	# region events

	public delegate void OnLifePointsModifiedDelegate 		();

	public static OnLifePointsModifiedDelegate				OnLifePointsModifiedHandler;

	# endregion

	private int _leftPlayerLifePoints;

	public int leftPlayerLifePoints {

		get { return _leftPlayerLifePoints; }
		set {

			_leftPlayerLifePoints = value;

			if (_leftPlayerLifePoints < 0) _leftPlayerLifePoints = 0;

			NotifyLifePointsModified();
		}
	}

	private int _rightPlayerLifePoints;
	
	public int rightPlayerLifePoints {
		
		get { return _rightPlayerLifePoints; }
		set {
			
			_rightPlayerLifePoints = value;

			if (_rightPlayerLifePoints < 0) _rightPlayerLifePoints = 0;

			NotifyLifePointsModified();
			
		}
	}

	public static CounterController Instance;

	void Awake () {

		Instance = this;

	}

	// Use this for initialization
	void Start () {
	
		ResetLifePoints ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnResetLifePointsButtonClicked () {
	
		// by now only reset
		ResetLifePoints ();
	}

	private void ResetLifePoints () {
		
		_leftPlayerLifePoints 	= INITAL_LIFE_POINTS;
		_rightPlayerLifePoints 	= INITAL_LIFE_POINTS;

		// throw event
		NotifyLifePointsModified ();
	}

	private void NotifyLifePointsModified() {

		if (OnLifePointsModifiedHandler != null) {
		
			OnLifePointsModifiedHandler();
		}
	}


}
