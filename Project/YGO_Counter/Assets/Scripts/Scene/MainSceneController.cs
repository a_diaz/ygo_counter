﻿using UnityEngine;
using System.Collections;

public class MainSceneController : MonoBehaviour {

	public enum PlayerID {
		
		Left = 0,
		Right = 1
	}

	# region constants

	public const int DEFAULT_TARGET_FRAMERATE 	= 24;

	public const string DEFAULT_LEFT_PLAYER_NAME = "Left Player";

	public const string DEFAULT_RIGHT_PLAYER_NAME = "Right Player";
	
	# endregion

	# region events

	public delegate void OnPlayerNamesModifiedDelegate 		();
	
	public static OnPlayerNamesModifiedDelegate				OnPlayerNamesModifiedHandler;

	# endregion


	# region pref key attributes

	public string targetFrameRatePrefKey;

	public string leftPlayerLastNamePrefKey;

	public string rightPlayerLastNamePrefKey;

	# endregion

	# region attributes

	private string _currentLeftPlayerName;

	public string currentLeftPlayerName {

		get { return _currentLeftPlayerName; }
		set {

			_currentLeftPlayerName = value;
			NotifyPlayerNamesUpdated ();
		}
	}

	private string _currentRightPlayerName;
	
	public string currentRightPlayerName {
		
		get { return _currentRightPlayerName; }
		set {
			
			_currentRightPlayerName = value;
			NotifyPlayerNamesUpdated ();
		}
	}
	
	public int currentTargetFrameRate {

		get { return Application.targetFrameRate; }
	}

	public static MainSceneController Instance;

	# endregion

	void Awake () {

		SetTargetFrameRate (LoadPrefTargetFrameRate());
		Instance = this;
	}

	# region player prefs

	private int LoadPrefTargetFrameRate () {
	
		return PlayerPrefs.GetInt (targetFrameRatePrefKey, DEFAULT_TARGET_FRAMERATE);
	}

	private void SavePrefTargetFrameRate (int target) {
	
		PlayerPrefs.SetInt (targetFrameRatePrefKey, target);
	}

	private string LoadPrefPlayerName (MainSceneController.PlayerID player) {
	
		switch (player) {
		
		case PlayerID.Left:
			return LoadPrefLeftPlayerName ();
		case PlayerID.Right: 
			return LoadPrefRightPlayerName ();
		default:
			return "";
		}
	}

	private string LoadPrefLeftPlayerName () {
	
		return PlayerPrefs.GetString (leftPlayerLastNamePrefKey, DEFAULT_LEFT_PLAYER_NAME);
	}

	private void SetPrefLeftPlayerName (string name) {
	
		PlayerPrefs.SetString (leftPlayerLastNamePrefKey, name);
	}

	private string LoadPrefRightPlayerName () {
		
	 	return PlayerPrefs.GetString (rightPlayerLastNamePrefKey, DEFAULT_RIGHT_PLAYER_NAME);
	}
	
	private void SetPrefRightPlayerName (string name) {
		
		PlayerPrefs.SetString (rightPlayerLastNamePrefKey, name);
	}

	# endregion

	# region utilites

	private void SetTargetFrameRate (int target) {
		
		Application.targetFrameRate = target;
	}

	private void NotifyPlayerNamesUpdated () {
	
		if (OnPlayerNamesModifiedHandler != null) {
		
			OnPlayerNamesModifiedHandler ();
		}
	}

	public void OnTargetFrameRatePreferenceModified (int target) {
	
		SetTargetFrameRate (target);
		SavePrefTargetFrameRate (target);
	}
	
	public void OnExitApplicationButtonClicked () {
		
		// by now only exit
		ExitApplication ();
	}
	
	private void ExitApplication () {
		
		Application.Quit ();
	}

	# endregion
}
