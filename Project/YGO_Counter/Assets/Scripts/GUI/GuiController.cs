﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GuiController : MonoBehaviour {

	public Text leftPlayerLifePointsText;
	public Text rightPlayerLifePointsText;

	public Text leftPlayerNameText;
	public Text rightPlayerNameText;

	public static GuiController Instance;

	void Awake () {
	
		Instance = this;
	}

	void OnEnable () {
	
		InitEvents ();
	}

	void OnDisable () {
	
		UnsuscribeFromAllEvents ();
	}

	# region events

	private void InitEvents () {

		CounterController.OnLifePointsModifiedHandler += UpdateAllLifePointsText;
		MainSceneController.OnPlayerNamesModifiedHandler += UpdateAllPlayerNamesText;
	}

	private void UnsuscribeFromAllEvents () {
	
		CounterController.OnLifePointsModifiedHandler -= UpdateAllLifePointsText;
		MainSceneController.OnPlayerNamesModifiedHandler -= UpdateAllPlayerNamesText;
	}

	# endregion



	# region set text labels

	private void UpdateAllLifePointsText () {
		
		SetLeftPlayerLifePointsText (CounterController.Instance.leftPlayerLifePoints);
		SetRightPlayerLifePointsText (CounterController.Instance.rightPlayerLifePoints);
	}

	public void SetLeftPlayerLifePointsText (int lifePoints) {

		leftPlayerLifePointsText.text = lifePoints.ToString();
	}

	public void SetRightPlayerLifePointsText (int lifePoints) {
		
		rightPlayerLifePointsText.text = lifePoints.ToString();
	}
	
	private void UpdateAllPlayerNamesText () {

		SetLeftPlayerNameText (name);
		SetRightPlayerNameText (name);
	}

	public void SetLeftPlayerNameText (string name) {
		
		leftPlayerNameText.text = name;
	}
	
	public void SetRightPlayerNameText (string name) {
		
		rightPlayerNameText.text = name;
	}

	# endregion

	# region button click

	public void OnResetLifePointsButtonClicked () {
	
		CounterController.Instance.OnResetLifePointsButtonClicked ();
	}

	public void OnExitApplicationButtonClicked() {
	
		MainSceneController.Instance.OnExitApplicationButtonClicked ();
	}

	# endregion

	# region increment and decrement button 

	public void OnLeftPlayerIncrementButtonClicked (int amount) {

		CounterController.Instance.leftPlayerLifePoints += amount;
	}

	public void OnLeftPlayerDrecrementButtonClicked (int amount) {

		CounterController.Instance.leftPlayerLifePoints -= amount;
	}

	public void OnRightPlayerIncrementButtonClicked (int amount) {
		
		CounterController.Instance.rightPlayerLifePoints += amount;
	}
	
	public void OnRightPlayerDrecrementButtonClicked (int amount) {
		
		CounterController.Instance.rightPlayerLifePoints -= amount;
	}

	# endregion

}
